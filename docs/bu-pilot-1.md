# Bitcoin Unlimited pilot vote #1

    Status: DRAFT

## Overview

The first BU vote using VotePeer is scheduled to happen in 2021.

## Q\&A

### Can we expect BU members to have an Android device?

No, there must be other options to vote.

### Do we use both voting systems in parallel?

Ideally, we want to use VotePeer, and maybe those who can't, will
have to use the forum for a manual vote.

#### If a user casts different votes on both systems, which vote is valid?

Systems being: 1) VotePeer, 2) Online forum

The vote on the blockchain takes precedence.

Note that this allows a forum voter to change his mind by using the VotePeer
application later.

A vote cast with VotePeer is final.

### Is it feasible to get the app on Apple's App Store?

Not for the first pilot run.

### How do we link a user and their VotePeer identity?

For the first pilot, users sign a message containing their VotePeer identity.
The message must be signed with their Bitcoin Unlimited identity.

The message must be `link:bitcoincash:qqXXXXX`. The signature is manually
submitted to the VotePeer database.

In the future we can create a system to let users link their accounts without
manual intervention.

### Shall who voted what be visible to the public as they happen?

The votes shall be visible when they happen.

If anonymous voting is used, a proof of why the vote is valid shall be made
available.

If transparent voting is used, who voted shall be visible as well as a proof
of why the vote is valid.

### How are the votes shown on the BU history page?

We create a new vote overview page. We link to the old one for viewing the
older votes.

We link from the old page to the new one.

#### Where is the new overview page hosted?

For the pilots, it will be developed and hosted on
`voter.cash/#bitcoinunlimited`. The election data is made available as JSON
data via RESTful API. This is to allow the vote to be integrated with BU's
main website.

### Will we allow anonymizing users when voting?

We will leave it up to the BUIP writer if users shall be anonymized or not.

Anonymous voting as implemented has a weakness that's not solved. Although
the vote itself does not expose who it belongs to, the contract it uses
needs to be funded. So it's possible to discover (and likely trivial) who
own the vote by blockchain analysis.

It is understood that this privacy leak exists.

## Process for creating the Election

1.  Select *Election Type* to preference of BUIP writer. If no preference is
    specified, use *Transparent*.

2.  In Description, the first line is the `Title` of the Election.
    The title is formatted as `BUIPXXX: Title of proposal`

    The second line is blank.

    The third line is the SHA256 hash of a document with the proposal. This can
    for example be a PDF file.

    The fourth line is a URL where the file can be downloaded.

3.  Set *First vote option* to "Accept"

4.  Set *Second vote option* to "Reject"

5.  Add the *VotePeer address* of Bitcoin Unlimited members as participants.
    Ensure there exists signature made with their actual Bitcoin Unlimited
    member address that proves this is their VotePeer address. Note that it
    is not possible to change participants after the election is created.

6.  Set election end time to when voting ends. The anonymous contract also
    supports setting begin time.

7.  Press \[ Create Election ] and verify that election was created.

### Example description:

    BUIP166 Launch Chain Proving Next Gen Features

    SHA256: dc0c3439c54fcbba4a52b9a317317ae3e0caf1a1e77cc43752e2bf99f524e7fa
    URL: https://www.bitcoinunlimited.info/voting/raw/raw_file/dc0c3439c54fcbba4a52b9a317317ae3e0caf1a1e77cc43752e2bf99f524e7fa
