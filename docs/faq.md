# Frequently Asked Questions (FAQ)

### Do I need the Android app to vote?
Yes

### Can I add a BCH address after the vote has started?
No

### Is it really anonymous or is it pseudonymous? 
_"Anonymous" implies the vote is truly private and can't be connected to your identity._

VotePeer uses traceable ring signature for signing a vote fully anonymously and can be used with CashFusion to make funding the transaction practically untraceable 

<hr>

### Is it possible to restrict who can vote while maintaining anonymity?
A list of public keys representing voters is known in advance. Everyone knows this list of voters.

You can't tell who from that list cast which vote. You can tell that a vote was cast by someone on that list.

<hr>


### What is to stop me from voting multiple times in a row? Votes today are based on kyc

Are you asking what is stopping you from technically voting multiple times, or what's stopping you from participating multiple times?

**Multiple votes from one identity**

Technically, everyone can see if you voted multiple times. If you have two or more votes in the same block, you spoil your vote. If you have two or more votes across multiple blocks, the one with the most confirmations is counted.

**Multiple identites**

Participants are added before the vote starts. Every participants needs to provide an Bitcoin Cash address representing their voter identity. It's up to the election "host/master" to decide whom to add or how to filter duplicate participants out. The contract is designed such that the list of all the participants is available to all other participants.
