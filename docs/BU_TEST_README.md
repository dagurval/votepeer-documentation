# VotePeer & Bitcoin Unlimited test-Vote 15-20 October 2021

***

This document is a guide to the Bitcoin Unlimited electorate. The Bitcoin Unlimited Vote 15-20 will have a parallel test-Vote using [VotePeer: Censorship resistant blockchain voting](https://voter.cash). The document is constrained by this user story:

*As a Bitcoin Unlimited member, I want to use VotePeer and the Bitcoin Unlimited Voting system in parallel when participating in the October 2021 Bitcoin Unlimited election.*

### **Prerequisites**

***

An android device with:

*   Google play installed
*   Android operating system 8.0 and up
*   Network access

A small amount of BCH for transaction fees.

A user at https://forum.bitcoinunlimited.info

### Setup

***

<a href="https://play.google.com/store/apps/details?id=org.wikiedufoundation.wikiedudashboard.release"><img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" height=60px /></a>

1.  Install the VotePeer from **[Google Play](https://play.google.com/store/apps/details?id=org.wikiedufoundation.wikiedudashboard.release)**.
2.  Open the app and navigate to menu->identity.
3.  Share address/identity to **[this thread](https://forum.bitcoinunlimited.info/t/announcing-the-next-bu-vote-15-20-october-2021/179/7)**.
4.  Fund the Identity with a small amount of BCH. VotePeer uses BCH smart contracts to store votes for future reference. Therefore voting in-app requires BCH funds.

### Voting

***

After being scheduled to receive a *push notification*  by signing up for the parallel election, Bitcoin Unlimited members will receive a push notification about the October test election on their Android devices.

Participants receive a push notification to notify them of inclusion into the electorate. Clicking the push notification opens the app, and the election is under the "Your Elections"-list.

Participants then click the new election in the list to navigate to it. If it has started and your identity has a small amount of BCH, you can immediately vote on-chain using the VotePeer smart contract. VotePeer stores your ballot on-chain on Bitcoin Cash using BCH smart-contracts.

After submitting a vote, it is recommended to try to "View in block explorer"-button and see what a voting transaction looks like on the Bitcoin Unlimited block explorer.

Navigate back to VotePeer because your vote is probably available in the on-chain tally! Press the "View Results" button to fetch election results directly from the Bitcoin Cash smart-contracts and into your VotePeer..

### Various Screenshots

| Identity | Election feed | Election |
| ------ | ------ | ------ |
| <img src="https://voter.cash/img/android-identity.accb0730.png" width="250"/> | <img src="https://voter.cash/img/android-election-list.8257d310.png" width="250"/> | <img src="https://voter.cash/img/android-submit-vote.a2d30cfb.png" width="250"/> |
| Used for sharing identity and receing small amounts of BCH for voting-tranction fees. | Your Elections | This user is still deciding... |
