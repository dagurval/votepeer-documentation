# VotePeer Documentation

## Introduction
Censorship resistant on-chain blockchain voting. This is the documentation for the VotePeer project. The project originated from [Bitcoin Unlimited proposal #129](https://bitco.in/forum/threads/buip129-passed-finish-and-productize-the-bu-voting-system.24367).

The fundamental innovation in Votepeer compared to traditional online voting and polling systems is that VotePeer stores each vote on the bitcoin cash blockchain instead of storing votes in a centralized database. Using this design principle, VotePeer supports both transparent and anonymous elections. In transparent elections, everyone can see what everyone else voted in real-time. Votepeer is now being upgraded to support anonymous elections where nobody can see what the other participants voted while still tallying the result directly from the bitcoin cash blockchain to the VotePeer android app.

Compared to a traditional ballot box where participants write their choice on a piece of paper and trust the election administration to count the vote. VotePeer ensures that users can verify that the tally included their vote by using the VotePeer android app to read the ballot status and tally directly from the bitcoin cash blockchain. Trust, but verify.

The VotePeer android app is essentially a bitcoin wallet that interacts with on-chain bitcoin cash smart contracts. Upon opening the VotePeer android app for the first time, a mnemonic is generated and stored on the user's phone. An address from the mnemonic is automatically selected as the user's VotePeer blockchain identity.

When creating a new vote, all participants bitcoin cash addresses are required. Nobody can join the vote after it has started. If a participant attempts to vote twice, both votes are invalidated, and the participant's vote is set to "spoilt" (Invalid).

## Demo: Anonymous vote on Bitcoin Cash

[![VotePeer YouTube Demo](http://img.youtube.com/vi/NBhE_v6yewo/0.jpg)](http://www.youtube.com/watch?v=NBhE_v6yewo "VotePeer: Anonymous vote on Bitcoin Cash demo")

## Demo: Transparent vote on Bitcoin Cash
| Election feed | Election |
| ------ | ------ |
| <img src="./election_list.jpg" width="420"/> | <img src="./election_detail.jpg" width="420"/> |
| Your Elections. | This has voted. |


## Research

#### [Two option vote contract for Bitcoin Cash](two-option-vote-contract.md) (2020)

#### [Voting on Bitcoin Cash using Traceable Ring Singatures](ring-signature-vote-contract.md) (2020)

#### [Transaction Input Payload Contract](input-payload-contract.md) (2021)

#### [Publishing Data on Bitcoin Cash Anonymously Using Faucets](anonymous-contract-funding.md) (2021)

#### [Multi option vote contract for Bitcoin Cash](multi-option-vote-contract.md) (2021)

## Developer Reference

#### [VotePeer API](votepeer-api.md)

## Software

#### [VotePeer Android App](https://voter.cash/)

App for participating in elections. [Git repository](https://gitlab.com/nerdekollektivet/buip129-android).

#### [WallyWallet](https://gitlab.com/wallywallet/android)

Bitcoin Cash Wallet with VotePeer integrated.

#### [VotePeer Election Creator](https://voter.cash/)

Web interface for creating and administering elections. [Git repository](https://gitlab.com/nerdekollektivet/buip129-website)

#### [VotePeer Androud Library](https://gitlab.com/nerdekollektivet/votepeer-library)

Library for integrating blockchain voting in Android projects.

#### [libbitcoincashkotlin](https://gitlab.com/bitcoinunlimited/libbitcoincashkotlin)

Bitcoin Cash library in kotlin (with batteries included philosophy). Low-level VotePeer functionality is implemented here, including ring signature primitives and Script contracts.

## Contact

*   [Telegram](http://t.me/buip129)
*   [Twitter](https://twitter.com/votepeer)
