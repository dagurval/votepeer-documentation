# Election tags

It is possible to assign *tags* to elections. Tags are used to filter elections
using API calls such as [list\_public\_elections](voterpeer-api).

Tag support was added to allow projects to integrate elections with their own
system, such as for allowing the organization Bitcoin Unlimited to create an
overview site of their member votes.

## Filtering on tags

Look for `filter_tags` parameter on [API calls](voterpeer-api). API calls
supporting this filter option will only return elections that contain
given tag.

## Assigning tags to an election

Users need to be assigned to tags they are allowed to use. If a user has been
assigned tags, these will show during in the new election form.

![Tags list in new election](election-tags-new-election.png)

## Giving users permission to use tags

There is no user interface for this developed. To assign tags to a user, use
the database interface directly. Add a `allowed_tags` field to a user document.
This field must be a array of strings.

![Tags in database](election-tags-firebase1.png)
![Tags in database](election-tags-firebase2.png)
